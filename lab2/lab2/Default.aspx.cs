﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lab2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void StartDate_SelectionChanged(object sender, EventArgs e)
        {
           // EndDate.SelectedDate = (sender as Calendar).SelectedDate;
        }

        protected void EndDate_SelectionChanged(object sender, EventArgs e)
        {
        }


        protected void Calc_Click(object sender, EventArgs e)
        {
            float count = float.Parse(Count.Text);
            float percent = float.Parse(Percent.SelectedValue);
            int dayCount = (EndDate.SelectedDate - StartDate.SelectedDate).Days;
            float resultPercent = count * percent / 100 * dayCount / 360;
            Output.InnerText = "Сумма процентов по кредиту: " + resultPercent;
            ShowPopUpMsg("Сумма процентов по кредиту: " + resultPercent);
        }
        private void ShowPopUpMsg(string msg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert('");
            sb.Append(msg.Replace("\n", "\\n").Replace("\r", "").Replace("'", "\\'"));
            sb.Append("');");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showalert", sb.ToString(), true);
        }
    }
}
