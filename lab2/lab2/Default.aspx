﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="lab2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <span>Сумма кредита</span>
        <asp:TextBox runat="server" ID="Count" TextMode="Number" />

    </div>
    <div class="row">
        <span>Процентная ставка</span>
        <asp:DropDownList runat="server"  ID="Percent">
            <asp:ListItem Text="12" />
            <asp:ListItem Text="15" />
            <asp:ListItem Text="20" />
        </asp:DropDownList>
    </div>
    <div class="row">
        <span>День выдачи кредита</span>
        <asp:Calendar ID="StartDate" runat="server" OnSelectionChanged="StartDate_SelectionChanged" />
    </div>
    <div class="row">
        <span>День погашения кредита</span>
        <asp:Calendar ID="EndDate" runat="server" OnSelectionChanged ="EndDate_SelectionChanged" />
    </div>
    <div class="row">
        <asp:Button Text="Посчитать" runat="server"  OnClick="Calc_Click" />
        <span id="Output"  runat="server"></span>
    </div>
</asp:Content>
