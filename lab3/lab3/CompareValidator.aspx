﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompareValidator.aspx.cs" Inherits="lab3.CompareValidator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:TextBox runat="server" ID="textBox1" />
            <asp:TextBox runat="server" ID="textBox2" />
            <asp:CompareValidator ErrorMessage="Value should be greter then in right textbox" ControlToValidate="textBox1" runat="server" Type="Integer" Operator="GreaterThan" ControlToCompare="textBox2" />
            <asp:Button Text="text" runat="server" ID="button1" />
        </div>
    </form>
</body>
</html>
