﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Validators.aspx.cs" Inherits="lab3.Validators" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server" class="border">
        <div>
            <asp:TextBox runat="server" ID="textBox" />
            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="textBox" runat="server" />
            <asp:RangeValidator ErrorMessage="Input number [0;10]" ControlToValidate="textBox" runat="server" Type="Integer" MaximumValue="10" MinimumValue="0" />
            <asp:Button Text="text" runat="server" ID="button" />
        </div>
    </form>
</body>

</html>
