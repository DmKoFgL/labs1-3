﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllValidators.aspx.cs" Inherits="lab3.AllValidators" MasterPageFile="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >

    <div class="container-fluid">
        <div class="row form-group">
            <asp:Label Text="Fist name" runat="server"  class="col-sm-4"  />
            <asp:TextBox runat="server"  class="col-sm-4" ID="FirstName"  />
              <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="FirstName" runat="server"  class="col-sm-4" />
        </div>
        <div class="row form-group">
            <asp:Label Text="Middle name" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="MiddleName" />
        </div>
            <div class="row form-group">
            <asp:Label Text="Last name" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="LastName" />
              <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="LastName" runat="server"  class="col-sm-4" />
        </div>
        <div class="row form-group">
            <asp:Label Text="Email" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="Email" TextMode="Email" />
          
        </div>
        <div class="row form-group">
            <asp:Label Text="Password" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="Password" TextMode="Password" />
            <asp:RegularExpressionValidator runat="server"  class="col-sm-4"
                ControlToValidate="Password" ValidationExpression="(\w{5,})([0-9]{1,})"
                ErrorMessage="Password is not in a valid format" Display="Dynamic" />
            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="Password" runat="server"  class="col-sm-4" />
        </div>
        <div class="row form-group">
            <asp:Label Text="Re-enter password" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="ReEnterPasword" TextMode="Password" />
            <asp:CompareValidator ErrorMessage="Value should be equal password" ControlToValidate="ReEnterPasword" runat="server"  class="col-sm-4" Type="Integer" Operator="Equal" ControlToCompare="Password" />
            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="ReEnterPasword" runat="server"  class="col-sm-4" />
        </div>
        <div class="row border">
            <asp:Label Text="Age" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="Age" TextMode="Number" />
            <asp:RangeValidator ErrorMessage="Input number [18;99]" ControlToValidate="Age" runat="server"  class="col-sm-4" Type="Integer" MaximumValue="99" MinimumValue="18" />
            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="Age" runat="server"  class="col-sm-4" />
        </div>
        <div class="row form-group">
            <asp:Label Text="Phone" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="Phone" TextMode="Phone" />
            <asp:RegularExpressionValidator runat="server"  class="col-sm-4"
                ControlToValidate="Phone" ValidationExpression="^\+\(\d{3}\)-\d{2}-\d{3}-\d{2}-\d{2}$"
                ErrorMessage="Must be in a valid format +(nnn)-nn-nnn-nn-nn" Display="Dynamic" />

            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="Phone" runat="server"  class="col-sm-4" />
        </div>
        <div class="row form-group">
            <asp:Label Text="Address" runat="server"  class="col-sm-4" />
            <asp:TextBox runat="server"  class="col-sm-4" ID="Address" Rows="5" TextMode="MultiLine"/>

            <asp:CustomValidator ErrorMessage="Input less then 20 symbols" ControlToValidate="Address" runat="server"  class="col-sm-4" ClientValidationFunction="longer20" />
            <script type="text/javascript">
                function longer20(ctl, args) {
                    args.IsValid = (args.Value.length > 20);
                }
            </script>
            <asp:RequiredFieldValidator ErrorMessage="Field can't be empty" ControlToValidate="Address" runat="server"  class="col-sm-4" />
        </div>
          <div class="row form-group center">
              <div  class="col-sm-1"></div>
        <asp:Button Text="Add" runat="server"  class="col-sm-2 btn btn-default" />
              <div  class="col-sm-1"></div>
        <asp:Button Text="Cancel" runat="server"  class="col-sm-2 btn btn-default" />
              </div>
    </div>
</asp:Content>
