﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product1.aspx.cs" Inherits="lab1_2_2.Product1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Lights</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <img src="~/Content/img/1.jpg" runat="server" alt="Alternate Text" />
            <span>Specificationss</span>
            <ul>
                <li>Name - Motor</li>
                <li>Price - 1000</li>
                <li>Currency - TUG</li>
                <li>Volume - 3.9</li>
                <li>Producer - SomeonE</li>
            </ul>
        </div>
    </form>
</body>
</html>
