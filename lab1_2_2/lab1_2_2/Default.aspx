﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="lab1_2_2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-fluid">
        <div class="col-xs-4">
            <a runat="server" href="/products/1">Lights</a>
            <img src="~/Content/img/1.jpg" runat="server" alt="Alternate Text" />
        </div>
        <div class="col-xs-4">
            <a runat="server" href="/products/2">Motors</a>
            <img src="~/Content/img/2.jpg" alt="Alternate Text" runat="server" />

        </div>
    </div>

</asp:Content>
